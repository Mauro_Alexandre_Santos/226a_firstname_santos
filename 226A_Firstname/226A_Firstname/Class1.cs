﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace FirstNameProject
{
    public class FirstName
    {
        string firstName1="", firstName2="";
        public FirstName(string firstName1, string firstName2)
        {
            if (firstName1 != "") this.firstName1 = ClearText(firstName1);
            if (firstName2 != "") this.firstName2 = ClearText(firstName2);
        }
        public string Concat() {
            if (firstName1 == "")
            {
                if (firstName2 == "")
                {
                    return "Unknow Firstname";
                }
                else
                {
                    return firstName2;
                }
            }
            else
            {
                if (firstName2 == "")
                {
                    return firstName1;
                }
                else
                {
                    if (firstName1 == firstName2)
                    {
                        return firstName1;
                    }
                    else
                    {
                        return firstName1 + "-" + firstName2;
                    }
                }
            }
        }
        static string ClearText(string strToClear)
        {
            string cleanString = ""; 
            foreach (char a in strToClear) {
                if (Char.IsLetter(a))
                {
                    cleanString = cleanString + a;
                }
            }
            return cleanString[0].ToString().ToUpper() + cleanString.Substring(1).ToLower();
        }
    }
}
