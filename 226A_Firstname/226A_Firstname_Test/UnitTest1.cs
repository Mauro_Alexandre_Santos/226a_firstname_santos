﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstNameProject
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// This test methode is designed to test a concatenation of two firstname
        /// </summary>
        [TestMethod]
        public void concat_Success()
        {
            //given
            string firstName1 = "Jean";//deux prénoms
            string firstName2 = "Paul";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Jean-Paul";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }
        /// <summary>
        /// This test methode is designed to test the formatting of firstname
        /// </summary>
        [TestMethod]
        public void formatText_Success()
        {
            //given
            string firstName1 = "JEAN";//deux prénoms
            string firstName2 = "paul";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Jean-Paul";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }
        [TestMethod]
        public void concat_EmptyValue_FirstValue_Success()
        {
            //given
            string firstName1 = "";//deux prénoms
            string firstName2 = "Paul";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Paul";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }

        [TestMethod]
        public void concat_EmptyValue_SecondValue_Success()
        {
            //given
            string firstName1 = "Jean";//deux prénoms
            string firstName2 = "";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Jean";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }

        [TestMethod]
        public void concat_EmptyValues_Success()
        {
            //given
            string firstName1 = "";//deux prénoms
            string firstName2 = "";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Unknow Firstname";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }
        [TestMethod]
        public void concat_DuplicateFirstname_Success()
        {
            //given
            string firstName1 = "Paul";//deux prénoms
            string firstName2 = "Paul";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Paul";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }
        [TestMethod]
        public void concat_DuplicateFirstnameCaseSensitive_Success()
        {
            //given
            string firstName1 = "Paul";//deux prénoms
            string firstName2 = "paul";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Paul";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }
        [TestMethod]
        public void illegal_character()
        {
            //given
            string firstName1 = "Je*an";//deux prénoms
            string firstName2 = "Paul";
            FirstName testObjConcat = new FirstName(firstName1, firstName2); //un objet Concat
            string expectedConcat = "Jean-Paul";
            string actualConcat = "";

            //when
            //je concatène
            actualConcat = testObjConcat.Concat();

            //then
            //J'obtiens les prénoms concaténés avec un trait d'union
            Assert.AreEqual(expectedConcat, actualConcat);
        }
    }
}
